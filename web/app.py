from flask import render_template, request, Flask


app = Flask(__name__)

@app.route("/")
@app.route("/index")

def name():
    return render_template('name.html')

@app.route("/index/<name>") #ties the root URL to index function so that index function is invoked when user enters http://localhost:5000/

def index(name):
    #print(name)
    if ".." in name or "~" in name:
        #render_template sends data  to a template and returns the HTML file
        return render_template("403.html")

    else:
        try:
            return render_template("name.html", name=name)
        except:
            return render_template("404.html")


@app.errorhandler(404)
def error_404(error):
    return render_template("404.html")

@app.errorhandler(403)
def error_403(error):
    return render_template("403.html")

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
